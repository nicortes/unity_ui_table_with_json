﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class Table : MonoBehaviour
{   
    // Clase para el JSON con los datos de la tabla
    [Serializable]
    public class UITable {
        public string Title { get; set; }
        public List<string> ColumnHeaders { get; set; }
        //public string[] ColumnHeaders { get; set; }
        public Dictionary<string, string>[] Data { get; set; }
    }

    // Prefabs
    [SerializeField] GameObject tableTitlePrefab;
    [SerializeField] GameObject columnNamePrefab;
    [SerializeField] GameObject rowValuePrefab;

    // GameObjects
    [SerializeField] GameObject canvasGameObject;
    [SerializeField] GameObject columnsAndRowsGameObject;
    [SerializeField] Text consoleText;

    // Variables para la cantidad de columnas
    int maxAmountOfDataPerRow;
    int amountOfColumns;
    int amountOfColumnsHeaders;

    private void Awake() {
        //LoadFile();
    }

    // Start is called before the first frame update
    void Start() {
        LoadFile();
    }

    // Update is called once per frame
    void Update() {
        //MonitorDirectory(filePath);
    }

    /*
     * INTENTO DE MÉTODO PARA DETECTAR AUTOMÁTICAMENTE SI EL ARCHIVO ERA MODIFICADO 
     * 
    private static void MonitorDirectory(string path) {

        FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();

        fileSystemWatcher.Path = path;

        fileSystemWatcher.Changed += FileSystemWatcher_Changed;

        fileSystemWatcher.EnableRaisingEvents = true;

    }

    private static void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e) {

        LoadFile();

    }
    */

    // Para leer el archivo JSON y deserializarlo
    public void LoadFile() {

        string filePath = Application.dataPath + "/StreamingAssets/JsonChallenge.json";

        string jsonString = ReadFile(filePath);

        switch (jsonString) {
            case "0":
                consoleText.text = "No existe el archivo.";
                return;
                break;
            case "1":
                consoleText.text = "No se pudo abrir el archivo.";
                return;
                break;
            default:
                consoleText.text = "Archivo cargado.";
                break;
        }


        //consoleText.text = consoleText.text + "salí del switch" + "-";

        UITable loadedData = null;

        try {
            loadedData = JsonConvert.DeserializeObject<UITable>(jsonString);
        } catch (Exception e) {
            consoleText.text = consoleText.text + e + "-";
        }
        
        //consoleText.text = consoleText.text + "json en una clase" + "-";

        /*
        if (loadedData != null) {
            consoleText.text = consoleText.text + loadedData.ColumnHeaders[0] + "-";
        } else {
            consoleText.text = consoleText.text + "loadedData es nulo" + "-";
        }
        */

        //consoleText.text = consoleText.text + "ya pasé el if" + "-";

        PrepareUITable(loadedData);

        //PrintData(jsonString, loadedData);

        DisplayData(loadedData);
    }

    // Establece cantidad de columnas de la tabla y el tamaño de cada celda
    private void PrepareUITable(UITable loadedData) {
        DetermineAmountOfColumns(loadedData);

        columnsAndRowsGameObject.GetComponent<GridLayoutGroup>().constraintCount = amountOfColumns;

        float newColumnsAndRowsWidth, newColumnsAndRowsHeight;

        DetermineWidthAndHeight(loadedData, amountOfColumns, out newColumnsAndRowsWidth, out newColumnsAndRowsHeight);

        columnsAndRowsGameObject.GetComponent<GridLayoutGroup>().cellSize = new Vector2(newColumnsAndRowsWidth, newColumnsAndRowsHeight);
    }

    // Determina el ancho y el alto que debe tener cada celda de la tabla
    private void DetermineWidthAndHeight(UITable loadedData, int amountOfColumns, out float newColumnsAndRowsWidth, out float newColumnsAndRowsHeight) {
        float columnsAndRowsWidth = columnsAndRowsGameObject.GetComponent<RectTransform>().rect.width;
        float columnsAndRowsHeight = columnsAndRowsGameObject.GetComponent<RectTransform>().rect.height;

        newColumnsAndRowsWidth = columnsAndRowsWidth / amountOfColumns;
        newColumnsAndRowsHeight = columnsAndRowsHeight / (loadedData.Data.Length + 1);// Se reparte el alto del Panel entre todas las filas con datos + la fila con los nombres de las columnas
    }

    // Retorna el número de columnas que debe tener la tabla
    void DetermineAmountOfColumns(UITable loadedData) {
        
        maxAmountOfDataPerRow = 0;
        for (int i = 0; i < loadedData.Data.Length; i++) {
            int amountOfData = 0;
            foreach (KeyValuePair<string, string> keyValuePair in loadedData.Data[i]) {
                amountOfData++;
            }
            if (amountOfData >= maxAmountOfDataPerRow) {
                maxAmountOfDataPerRow = amountOfData;
            }
        }

        amountOfColumns = 0;
        amountOfColumnsHeaders = loadedData.ColumnHeaders.Count;

        if (amountOfColumnsHeaders > maxAmountOfDataPerRow) {
            amountOfColumns = amountOfColumnsHeaders;
        } else {
            amountOfColumns = maxAmountOfDataPerRow;
        }

        if (maxAmountOfDataPerRow > amountOfColumnsHeaders) {
            consoleText.text = "Puede que este archivo contenga datos que no pertenecen a ninguna columna.";
        }

    }

    // Trata de abrir un archivo y devuelve un ID si no puede abrirlo
    string ReadFile(string path) {
        if (File.Exists(path)) {
            try {
                return File.ReadAllText(path);
            } catch {
                return "1";
            }
        } else {
            return "0";
        }
    }

    // Imprime información relevante en la consola para vista del desarrollador
    private static void PrintData(string jsonString, UITable loadedData) {
        print(jsonString);

        print(loadedData);
        print(loadedData.Title);

        foreach (string column in loadedData.ColumnHeaders) {
            print(column);
        }

        for (int i = 0; i < loadedData.Data.Length; i++) {
            foreach (KeyValuePair<string, string> keyValuePair in loadedData.Data[i]) {
                print("Key: " + keyValuePair.Key + " Value: " + keyValuePair.Value);
            }
        }
        
        /*
        foreach (Data data in loadedData.Data) {
            print(data.ID);
            print(data.Name);
            print(data.Role);
            print(data.Nickname);
        }
        */

        /*
        for (int i = 0; i < loadedData.Data.Length; i++) {
            print(loadedData.Data[i].ID);
            print(loadedData.Data[i].Name);
            print(loadedData.Data[i].Role);
            print(loadedData.Data[i].Nickname);
        }
        */
    }

    // Toma los datos obtenidos del JSON y los muestra en una tabla creada con el sistema de UI de Unity
    void DisplayData(UITable loadedData) {
        CleanTable();
        DisplayTableTitle(loadedData);
        DisplayColumnNames(loadedData);
        DisplayRows(loadedData);
    }

    private void DisplayRows(UITable loadedData) {

        /*DEMASIADOS CICLOS ANIDADOS; NO ES LO MÁS ÓPTIMO*/

        // FILAS
        for (int i = 0; i < loadedData.Data.Length; i++) {

            int j = 0;

            /// VALORES DE LAS FILAS
            foreach (KeyValuePair<string, string> keyValuePair in loadedData.Data[i]) {

                while (j < loadedData.ColumnHeaders.Count) {
                    //for (int j = 0; j < loadedData.ColumnHeaders.Length;j++) { 

                    GameObject rowValueGameObject = Instantiate(rowValuePrefab);
                    //rowValueGameObject.transform.SetParent(rowGameObject.transform, false);
                    rowValueGameObject.transform.SetParent(columnsAndRowsGameObject.transform, false);
                    Text rowValueText = rowValueGameObject.GetComponent<Text>();

                    if (keyValuePair.Key == loadedData.ColumnHeaders[j]) {
                        rowValueText.text = keyValuePair.Value;
                        j++;
                        break;
                    } else {
                        rowValueText.text = "";
                        j++;
                    }
                }
            }

            // Si se acabaron los datos, pero aún quedan columnas no revisadas, se llenan con espacios en blanco
            while(j < loadedData.ColumnHeaders.Count){
                GameObject rowValueGameObject = Instantiate(rowValuePrefab);
                //rowValueGameObject.transform.SetParent(rowGameObject.transform, false);
                rowValueGameObject.transform.SetParent(columnsAndRowsGameObject.transform, false);
                Text rowValueText = rowValueGameObject.GetComponent<Text>();
                rowValueText.text = "";
                j++;
            }
        }
    }

    private void DisplayColumnNames(UITable loadedData) {

        for (int i = 0; i < loadedData.ColumnHeaders.Count; i++) {
            GameObject columnNameGameObject = Instantiate(columnNamePrefab);
            columnNameGameObject.transform.SetParent(columnsAndRowsGameObject.transform, false);
            Text columnNameText = columnNameGameObject.GetComponent<Text>();
            columnNameText.text = loadedData.ColumnHeaders[i];
        }
        
        // Si hay más datos que nombres de columnas, se rellena con espacios vacíos
        int temp = amountOfColumnsHeaders;
        while (temp < amountOfColumns) {
            GameObject columnNameGameObject = Instantiate(columnNamePrefab);
            columnNameGameObject.transform.SetParent(columnsAndRowsGameObject.transform, false);
            Text columnNameText = columnNameGameObject.GetComponent<Text>();
            columnNameText.text = "";
            loadedData.ColumnHeaders.Add("");
            temp++;
        }
    }

    private void DisplayTableTitle(UITable loadedData) {
        // Se crea una instancia GameObject del prefab
        GameObject tableTitleGameObject = Instantiate(tableTitlePrefab);

        // Se le asigna un GameObject padre
        tableTitleGameObject.transform.SetParent(canvasGameObject.transform, false);

        // Se toma el componente Text del GameObject creado
        Text tableTitleText = tableTitleGameObject.GetComponent<Text>();

        // Se referencia a la parte que tiene que ver con el texto y se asigna el título
        tableTitleText.text = loadedData.Title;
    }

    private static void CleanTable() {
        GameObject[] instantiatedGameObjects = GameObject.FindGameObjectsWithTag("InstantiatedGameObject");

        if (instantiatedGameObjects.Length > 0) {
            foreach (GameObject instantiatedGameObject in instantiatedGameObjects) {
                Destroy(instantiatedGameObject);
            }
        }
    }
}
