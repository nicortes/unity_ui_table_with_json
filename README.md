## Description
This Unity project reads a JSON file and shows a table (made with Unity UI components) on screen with the JSON data.

It works with different amounts of rows and columns, as long as the following is specified: Title, ColumnHeaders and Data.

# IMPORTANT: Depending on the way you test this project, the JSON file to modify will be different:

- If you want to test it using the Unity engine, the file to read will be "Assets/StreamingAssets/JsonChallenge.json".

- If you want to test it using the executable build (found in "Build/Unity_UI_Table_with_JSON.exe"), the file to read will be "Build/Unity_UI_Table_with_JSON_Data/StreamingAssets/JsonChallenge.json".

### This project uses the "JSON.NET for Unity" package, available for free in the Unity Asset Store.